window.ee = new EventEmitter();

var App = React.createClass({
    render: function () {
        return (
            <div className="row">
                <div className="col-md-4 col-sm-4 col-xs-4" id="form">
                    <Form />
                </div>
                <div className="col-md-8 col-sm-8 col-xs-8">
                    <Table />
                </div>
            </div>
        )
    }
})


var Form = React.createClass({
    getInitialState: function () {
        return {
            emptyName: true,
            emptyPrice: true,
            emptyCount: true,
            editMode: false
        }
    },
    componentDidMount: function(){
        self = this;
        window.ee.addListener('item.edit', function(item){

            self.setState({
                emptyName: false,
                emptyPrice: false,
                emptyCount: false,
                editMode: true
            })
            var name = ReactDOM.findDOMNode(self.refs.name);
            var price = ReactDOM.findDOMNode(self.refs.price);
            var count = ReactDOM.findDOMNode(self.refs.count);

            name.value = item.name;
            price.value = item.price;
            count.value = item.count;
        })
    },
    componentWillUnmount: function(){
        window.ee.removeListener('item.edit');
    },
    onFieldChange: function (fieldName, e) {
        if (e.target.value.trim().length > 0) {
            this.setState({ ['' + fieldName]: false })
        } else {
            this.setState({ ['' + fieldName]: true })
        }
    },
    handleCreateOrUpdate: function () {
        var name = ReactDOM.findDOMNode(this.refs.name);
        var price = ReactDOM.findDOMNode(this.refs.price);
        var count = ReactDOM.findDOMNode(this.refs.count);

        var item = [{
            name: name.value,
            price: price.value,
            count: count.value
        }]

        if(this.state.editMode){
            window.ee.emit('item.update', item[0]);
        } else {
            window.ee.emit('item.add', item);
        }

        name.value = "";
        price.value = "";
        count.value = "";

        this.setState({
            emptyName: true,
            emptyPrice: true,
            emptyCount: true,
            editMode: false
        })

    },
    render: function () {
        var emptyCount = this.state.emptyCount;
        var emptyPrice = this.state.emptyPrice;
        var emptyName = this.state.emptyName;
        return (
            <form>
                <div className="form-group">
                    <input className="form-control" type='text' placeholder='Название' onChange={this.onFieldChange.bind(this, 'emptyName')} ref='name' />
                </div>
                <div className="form-group">
                    <input className="form-control" type='number' placeholder='Цена' onChange={this.onFieldChange.bind(this, 'emptyPrice')} ref='price' />
                </div>
                <div className="form-group">
                    <input className="form-control" type='number' placeholder='Количество' onChange={this.onFieldChange.bind(this, 'emptyCount')} ref='count' />
                </div>
                <button className="btn btn-default" disabled={emptyCount || emptyPrice || emptyName}
                    onClick={this.handleCreateOrUpdate}>
                    {this.state.editMode ? 'Изменить' : 'Добавить'}
                </button>
            </form>
        )
    }
})

var Table = React.createClass({
    getInitialState: function () {
        return {
            table: []
        }
    },

    componentDidMount: function () {
        if(localStorage.getItem('table')){
            this.setState({
                table: JSON.parse(localStorage.getItem('table'))
            });
        }

        var self = this;
        window.ee.addListener('item.add', function (item) {
            var newTable = item.concat(self.state.table);
            localStorage.setItem('table', JSON.stringify(newTable));
            self.setState({ table: newTable });
        });

        window.ee.addListener('item.delete', function(item){
            var newTable = self.state.table.filter(function(e){return e != item})
            localStorage.setItem('table', JSON.stringify(newTable));
            self.setState({ table: newTable });
        });

        window.ee.addListener('item.update', function(item){
            var index = localStorage.getItem('editing_el_id');
            
            var newTable = [].concat(self.state.table);
            newTable[index] = item;
            localStorage.setItem('table', JSON.stringify(newTable));
            self.setState({ table: newTable });
        })
    },
    componentWillUnmount: function () {
        window.ee.removeListener('item.add');
        window.ee.removeListener('item.delete');
        window.ee.removeListener('item.update');
    },
    render: function () {
        var tableData = this.state.table.map(function (item, index) {
            item.id = index;
            return (
                <TableItem data={item} key={index} />
            )
        })
        return (
            <table className="table table-striped table-bordered">
                <tbody>
                    <tr>
                        <th>Название</th>
                        <th>Цена</th>
                        <th>Количество</th>
                        <th>Сумма</th>
                        <th>Действия</th>
                    </tr>

                    {tableData}
                </tbody>
            </table>
        )
    }
})

var TableItem = React.createClass({
    getInitialState: function () {
        return {
            name: '',
            price: '',
            count: ''
        }
    },
    editItem: function(obj, e){
        localStorage.setItem('editing_el_id', obj.id);
        window.ee.emit('item.edit', obj)
    },
    deleteItem: function(obj, e){
        window.ee.emit('item.delete', obj)
    },
    render: function () {
        var obj = this.props.data;
        return (
            <tr>
                <td>{obj.name}</td>
                <td>{obj.price}</td>
                <td>{obj.count}</td>
                <td>{obj.count * obj.price}</td>
                <td>
                    <span className="my-icons glyphicon glyphicon-pencil im-showOrig"
                            onClick={this.editItem.bind(this, obj)}></span>
                    <span className="my-icons glyphicon glyphicon-remove im-showOrig"
                            onClick={this.deleteItem.bind(this, obj)}></span>
                </td>
            </tr>
        )
    }
})


ReactDOM.render(
    <App />,
    document.getElementById('root')
)